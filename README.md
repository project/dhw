# Disable Htaccess Writer

It disables the `htaccess_writer` core service, this is useful when you are running Drupal on a non-apache server like OpenLiteSpeed and it has incompatibilities with the Thumbnail styles core module

**WARNING:** This exposes your system to attackers, be sure to implement homologous methods on your web server to protect the directories