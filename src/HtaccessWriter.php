<?php

namespace Drupal\dhw;

use Drupal\Core\StreamWrapper\StreamWrapperManager;

/**
 * Provides functions to manage Apache .htaccess files.
 */
class HtaccessWriter extends \Drupal\Core\File\HtaccessWriter {

  public function write($directory, $deny_public_access = TRUE, $force_overwrite = FALSE) {
    if (StreamWrapperManager::getScheme($directory)) {
      $directory = $this->streamWrapperManager->normalizeUri($directory);
    }
    else {
      $directory = rtrim($directory, '/\\');
    }
    $file = $directory . DIRECTORY_SEPARATOR . '.htaccess';
    if (file_exists($file)) {
      unlink($file);
    }
    return TRUE;
  }

}