<?php

namespace Drupal\dhw;

/**
 * Provides functions to manage Apache .htaccess files.
 */
class HtaccessWriterCore extends \Drupal\Core\File\HtaccessWriter {

  public function write($directory, $deny_public_access = TRUE, $force_overwrite = FALSE) {
    return parent::write($directory, $deny_public_access, $force_overwrite);
  }

}